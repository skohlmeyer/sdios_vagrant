#!/bin/bash
apt-get update
apt-get install -y build-essential unzip automake autoconf bison flex mkisofs qemu minicom vim tree tmux git
su -l vagrant /vagrant/install.sh

cat << 'EOF' > /usr/local/bin/qemu
#!/bin/sh
exec /usr/bin/qemu-i386
EOF
chmod +x /usr/local/bin/qemu
